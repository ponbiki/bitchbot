#!/usr/bin/php -q

<?php
//require_once 'dbbitch.php';
//include the IRC Connection class
require_once('class/IRCConnection.class.php');

$server = 'irc.7chan.org'; //irc server address
$port = 6667; // port usually 6667 or 6697 for ssl
$botnick = 'Richard_Stallman'; // irc nick
$botname = 'johnny5'; //irc ident field
$realname = 'I forgot'; //irc ident field
$nspass = 'Pa55word'; //nickserv pass
$ownerid = 'ponbiki!asdf@I.is.confused'; // owner 
$channel = '#BitchBot';  // initial join channel
$distUrl = 'http://distrowatch.com/news/dwd.xml';  //new distro feed xml url
$dbuser = 'BitchBot'; //dbase username
$dbpass = 'P3n15'; //dbpass
$dbhost = 'localhost'; //change if remote db
$dbname = 'bitchbot'; //dbase name
$victim = 'steve'; //put the irc nick (in all lowercase) of the person you want to not like you
$ragelevel = '0'; //percentage of attack expressed as an integer between 0 and 100
$switch = 1; //force true value used for infinite loop
//force unlimited time to run
set_time_limit(0);

//Create new IRC connection
$irc = new IRCConnection($server,$port);

//auth
$irc->Send("USER " . $botname . " * 8 :" . $realname);
sleep(4); //temp solution to allow time for ircd needs replaced with proper command queue
$irc->Send("NICK " . $botnick);

//join
sleep(4); //temp solution same as above
$irc->Msg("NickServ", "IDENTIFY " . $nspass);
$irc->Join($channel);

//infinite looooop
while ($switch) {
    while ($data = $irc->Read(128)) {
        // nl2br if outputting to browser
        echo (php_sapi_name() == 'cli')?$data:nl2br($data);
        flush();
        //seperate the datas
        $ex = explode(' ', $data);
        //pingpong
        if ($ex[0] == "PING") {
            $irc->Send("PONG " . $ex[1]);
        }

        // looks for command keywords amongst the raw irc input
        $command = str_replace(array(chr(10), chr(13)), '', $ex[3]);

        //autojoin on channel invite
        if ($ex[1] == "INVITE" && $ex[2] == $botnick)
            $irc->Join(trim($ex[3], ":"));

        // allows universally accessible commands
        if ($ex[0] != ":" . $ownerid || $ex[0] == ":" . $ownerid) {
            switch ($command) {
                case ":;newdist":
                    $distStr = file_get_contents($distUrl);
                    $distObj = simplexml_load_string($distStr);
                    $arrXml = objectsIntoArray($distObj);
                    for ($j = 0; $j < 4; $j++) {
                        $irc->Msg($ex[2], $arrXml[channel][item][$j][title] . " " . $arrXml[channel][item][$j][link]);
                        $irc->Msg($ex[2], substr($arrXml[channel][item][$j][description], 0, 120));
                    }
                    break;
                case ":;ping":
                    exec(escapeshellcmd("ping -c 3 " . trim($ex[4])), $output);
                    foreach ($output as $msg) {
                        $irc->Msg($ex[2], $msg);
                        $output = "";
                    }
                    break;
                case":;nap":
                    preg_match("/^:([^!]+)!/", $ex[0], $match);
                    $irc->Msg($ex[2],"Get bent " . $match[1]);
                    break;
            }
        }
        // harass $victim
        preg_match("/^:([^!]+)!/", $ex[0], $match);

        if (strtolower($match[1]) == $victim) {
            if (rand(0, 99) < $ragelevel) {
                $db_server = mysql_connect($dbhost, $dbuser, $dbpass);
                if (!$db_server)
                    die(mysql_error());
                mysql_select_db($dbname) or die(mysql_error());
                $query = "SELECT * FROM insults";
                $result = mysql_query($query);
                if (!$result)
                    die(mysql_error());
                $rows = mysql_num_rows($result);
                $insultnum = rand(0, $rows - 1);
                $str = mysql_result($result, $insultnum, 'insult');
                mysql_close($db_server);
                $str = str_replace('$victim', $victim, $str);
                $str = str_replace('$shout', strtoupper($victim), $str);
                $irc->Msg($ex[2], $str);
            }
        }

        if ($ex[0] == ":" . $ownerid) {    // only allows auth user to command
            switch ($command) {
                case ":!join": 
                    $irc->Join($ex[4]);
                    break;
                case ":!part": 
                    $irc->Send("PART " . $ex[4] . " :" . implode(' ', array_slice($ex, 5)));
                    break;
                case ":!quit": 
                    $irc->Send( "QUIT :" . implode(' ', array_slice($ex, 4)));
                    $switch = 0;
                    break;
                case ":!nick";
                    $irc->Send("NICK :" . $ex[4]);
                    break;
                case ":!say": 
                    $irc->Msg($ex[4], implode(' ', array_slice($ex, 5)));
                    break;
                case ":!do": 
                    $irc->Msg($ex[4], chr(1) . "ACTION " . implode(' ', array_slice($ex, 5)) . "" . chr(1));
                    break;
                case ":!notice": 
                    $irc->Send("NOTICE " . $ex[4] . " :" . implode(' ', array_slice($ex, 5)));
                    break;
                case ":!mode": 
                    $irc->Send("MODE " . $ex[4] . " :" . implode(' ', array_slice($ex, 5)));
                    break;
                case ":!kick": 
                    $irc->Send("KICK " . $ex[4] . " " . $ex[5] . " :" . implode(' ', array_slice($ex, 6)));
                    break;
                case ":!memo": 
                    $irc->Msg('MemoServ', "SEND " . $ex[4] . " " . implode(' ', array_slice($ex, 5)));
                    break;
                case ":!badmemo":
                    $pid = pcntl_fork();
                    if ($pid == 0) {
                        for ($j = 0; $j < $ex[4]; $j++) { //evil loop to spam mail $ex[4] is spam #
                            $irc->Msg('MemoServ', "SEND " . $ex[5] . " " . implode(' ', array_slice($ex, 6)));
                            sleep(63);
                        }
                        posix_kill(getmypid(), 9);
                    }
                    break;
                case ":!flood": 
                    $pid = pcntl_fork();
                    if ($pid == 0) {
                        for ($j = 0; $j < $ex[5]; $j++) { //evil loop $ex[5] as spam# $ex[6] as delay
                            $irc->Msg($ex[4], implode(' ', array_slice($ex, 7)));
                            sleep($ex[6] / 10);
                        }
                        posix_kill(getmypid(), 9);
                    }
                    break;
                case ":!info": 
                    $irc->Msg($ex[2], exec("uname -a"));
                    $irc->Msg($ex[2], exec("uptime"));
                    break;
                case ":!target": 
                    $victim = $ex[4];
                    $ragelevel = $ex[5];
                    break;
                case ":!insult": 
                    $db_server = mysql_connect($dbhost, $dbuser, $dbpass);
                    $insult = mysql_real_escape_string(implode(' ', array_slice($ex, 4)));
                    if (!$db_server)
                        die(mysql_error());
                    mysql_select_db($dbname) or die(mysql_error());
                    $query = "INSERT INTO insults(insult) VALUES('$insult')";
                    $result = mysql_query($query);
                    if (!$result)
                        die(mysql_error());
                    mysql_close($db_server);
                    $irc->Msg($ex[2], "Ahhh yeah boy, that's added now.");
                    break;
            }
        }
    }
}

function objectsIntoArray($arrObjData, $arrSkipIndices = array()) {
    $arrData = array();

    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }

    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}
?>
