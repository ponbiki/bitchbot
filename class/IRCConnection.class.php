<?php
/**
 * Description of IRCConnection
 *
 * @author Steven
 */
class IRCConnection {

    private $socket;

    // IRCConnection constructor, will open the connection and such.
    function IRCConnection($host, $port = 6667, $ssl = false) {
        $this->socket = fsockopen($host, $port);
    }

    //Send a message to the irc server
    function Send($message) {
        fputs($this->socket, $message."\n");
    }

    //Read from the irc server
    function Read($length = null) {
        return fgets($this->socket, $length);
    }
    
    //Join a channel with optional key
    function Join($channel, $key = ''){
        return $this->Send("JOIN $channel $key");
    }
    
    function Msg($target,$message){
        return $this->Send("PRIVMSG $target :$message");
    }

}

?>
